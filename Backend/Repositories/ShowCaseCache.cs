﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using Backend.Models.Dtos;
using Backend.Models.RedisModels;
using Backend.Models.ResponseModels;
using Backend.Utilities;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Backend.Repositories
{
    public class ShowCaseCache
    {
        private readonly IDistributedCache _distributedCache;
        private readonly AssetToByteConverter _assetToByteConverter;
        private readonly Dictionary<int, string> _showCaseIds = new Dictionary<int, string>();
        private readonly IMapper _mapper;
        private int _localId = 1;

        public ShowCaseCache(IDistributedCache distributedCache, AssetToByteConverter assetToByteConverter, IMapper mapper)
        {
            _distributedCache = distributedCache;
            _assetToByteConverter = assetToByteConverter;
            _mapper = mapper;
        }

        public async void LoadCache()
        {
            var metadata = JsonConvert.DeserializeObject<IEnumerable<ShowCaseResponseModel>>(System.IO.File.ReadAllText(@".\Data\showcase.json"));

            foreach (var model in metadata)
            {
                var exists = await _distributedCache.ExistsAsync(model.Id);
                if (exists)
                {
                    _showCaseIds.Add(_localId++, model.Id);
                    continue;
                }

                var toAdd = _mapper.Map<ShowCaseModel>(model);

                foreach (var image in toAdd.CardImages)
                {
                    var raw = await _assetToByteConverter.ConvertToBytesAsync(image.Url);
                    if (raw == null) continue;
                    await _distributedCache.SetAsync(image.Id.ToString(), raw);
                }

                foreach (var image in toAdd.KeyArtImages)
                {
                    var raw = await _assetToByteConverter.ConvertToBytesAsync(image.Url);
                    if (raw == null) continue;
                    await _distributedCache.SetAsync(image.Id.ToString(), raw);
                }

                foreach (var video in toAdd.Videos)
                {
                    var raw = await _assetToByteConverter.ConvertToBytesAsync(video.Url);
                    if (raw == null) continue;
                    await _distributedCache.SetAsync(video.Id.ToString(), raw);

                    foreach (var alternativeVideo in video.Alternatives)
                    {
                        var alternativeRaw = await _assetToByteConverter.ConvertToBytesAsync(alternativeVideo.Url);
                        if (alternativeRaw == null) continue;
                        await _distributedCache.SetAsync(alternativeVideo.Id.ToString(), alternativeRaw);
                    }
                }

                var key = _localId++;
                toAdd.OrderNo = key;
                await _distributedCache.SetAsync(toAdd.Id, toAdd);
                _showCaseIds.Add(key, toAdd.Id);
            }
        }

        public async Task<IEnumerable<ShowCaseModel>> GetAllShowCases()
        {
            return await Task.WhenAll(_showCaseIds.Select(id => _distributedCache.GetAsync<ShowCaseModel>(id.Value)));
        }

        public async Task<IEnumerable<ShowCaseDto>> GetShowCases(int page, int size)
        {
            return await Task.WhenAll(_showCaseIds.OrderBy(x => x.Key).Skip(page * size).Take(size).Select(async x =>
            {
                var model = await _distributedCache.GetAsync<ShowCaseModel>(x.Value);
                return _mapper.Map<ShowCaseDto>(model);
            }));
        }

        public async Task<ShowCaseDto> GetShowCaseById(string id)
        {
            var model = await _distributedCache.GetAsync<ShowCaseModel>(id);

            return model == null ? null : _mapper.Map<ShowCaseDto>(model);
        }

        public async Task<string> GetMediaById(Guid id)
        {
            if (id == Guid.Empty) return null;

            var media = await _distributedCache.GetAsync(id.ToString());

            return media == null ? null : Convert.ToBase64String(media);
        }
    }
}
