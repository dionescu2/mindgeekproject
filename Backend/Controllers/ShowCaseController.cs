﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models.Dtos;
using Backend.Models.ResponseModels;
using Backend.Repositories;
using Backend.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowCaseController : ControllerBase
    {
        private readonly ShowCaseCache _showCaseCache;

        public ShowCaseController(ShowCaseCache showCaseCache)
        {
            _showCaseCache = showCaseCache;
        }

        [HttpGet]
        public async Task<ActionResult> GetFrontPage(int page, int size, bool morePages)
        {
            var toReturn = await _showCaseCache.GetShowCases(page, size);

            foreach (var showCaseDto in toReturn)
            {
                var biggestCardImageFound = false;
                foreach (var image in showCaseDto.CardImages.OrderByDescending(x => x.H).ThenByDescending(x => x.W))
                {
                    if (biggestCardImageFound) break;
                    var file = await _showCaseCache.GetMediaById(image.Id);
                    if (string.IsNullOrEmpty(file)) continue;
                    biggestCardImageFound = true;
                    image.File = file;
                }

                var biggestKeyArtImageFound = false;
                foreach (var image in showCaseDto.KeyArtImages.OrderByDescending(x => x.H).ThenByDescending(x => x.W))
                {
                    if (biggestKeyArtImageFound) break;
                    var file = await _showCaseCache.GetMediaById(image.Id);
                    if (string.IsNullOrEmpty(file)) continue;
                    biggestKeyArtImageFound = true;
                    image.File = file;
                }
            }

            if (toReturn.Any())
                return this.Ok(toReturn);

            return this.NotFound();
        }

        [HttpGet("GetShowCaseById")]
        public async Task<ActionResult> GetShowCaseById(string id)
        {
            var showCaseDto = await _showCaseCache.GetShowCaseById(id);

            if (showCaseDto == null)
                return NotFound();

            var biggestCardImageFound = false;
            foreach (var image in showCaseDto.CardImages.OrderByDescending(x => x.H).ThenByDescending(x => x.W))
            {
                if (biggestCardImageFound) break;
                var file = await _showCaseCache.GetMediaById(image.Id);
                if (string.IsNullOrEmpty(file)) continue;
                biggestCardImageFound = true;
                image.File = file;
            }

            var biggestKeyArtImageFound = false;
            foreach (var image in showCaseDto.KeyArtImages.OrderByDescending(x => x.H).ThenByDescending(x => x.W))
            {
                if (biggestKeyArtImageFound) break;
                var file = await _showCaseCache.GetMediaById(image.Id);
                if (string.IsNullOrEmpty(file)) continue;
                biggestKeyArtImageFound = true;
                image.File = file;
            }

            foreach (var video in showCaseDto.Videos)
            {
                var videoFile = await _showCaseCache.GetMediaById(video.Id);

                if (!string.IsNullOrEmpty(videoFile))
                    video.File = videoFile;

                var highestQualityVideoFound = false;
                foreach (var alternativeVideo in video.Alternatives.OrderByDescending(x => x.Quality))
                {
                    if (highestQualityVideoFound) break;
                    var file = await _showCaseCache.GetMediaById(alternativeVideo.Id);
                    if (string.IsNullOrEmpty(file)) continue;
                    highestQualityVideoFound = true;
                    alternativeVideo.File = file;
                }
            }

            return this.Ok(showCaseDto);
        }

        [HttpGet("GetMediaById")]
        public async Task<ActionResult> GetMediaById(Guid id)
        {
            var toReturn = await _showCaseCache.GetMediaById(id);

            if (toReturn != null)
                return this.Ok(toReturn);

            return this.NotFound();
        }
    }
}