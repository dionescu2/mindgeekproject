﻿namespace Backend.Utilities
{
    public static class StringExtensions
    {
        public static QualityType ToQualityType(this string str)
        {
            switch (str)
            {
                case "Low":
                    return QualityType.Low;
                case "Medium":
                    return QualityType.Medium;
                case "High":
                    return QualityType.High;
            }

            return QualityType.Unknown;
        }
    }
}
