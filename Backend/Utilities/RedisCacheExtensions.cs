﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace Backend.Utilities
{
    public static class RedisCacheExtensions
    {
        public static async Task SetAsync<T>(this IDistributedCache distributedCache, string key, T value, CancellationToken token = default(CancellationToken))
        {
            var options = new DistributedCacheEntryOptions();

            await distributedCache.SetAsync(key, value.ToByteArray(), options, token);
        }

        public static async Task<T> GetAsync<T>(this IDistributedCache distributedCache, string key, CancellationToken token = default(CancellationToken)) where T : class
        {
            var result = await distributedCache.GetAsync(key, token);
            return result.FromByteArray<T>();
        }

        public static async Task<bool> ExistsAsync(this IDistributedCache distributedCache, string key)
        {
            var exists = await distributedCache.GetAsync(key);
            return exists != null;
        }
    }
}
