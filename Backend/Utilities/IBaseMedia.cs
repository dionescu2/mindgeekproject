﻿using System;

namespace Backend.Utilities
{
    public interface IBaseMedia
    {
        Guid Id { get; set; }
        string File { get; set; }
    }
}
