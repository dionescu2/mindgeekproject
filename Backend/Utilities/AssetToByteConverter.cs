﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Backend.Models.RedisModels;
using Backend.Models.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Utilities
{
    public class AssetToByteConverter
    {
        public async Task<byte[]> ConvertToBytesAsync(string url)
        {
            if (url == null)
                return null;

            try
            {
                var webClient = new WebClient();
                return await webClient.DownloadDataTaskAsync(new Uri(url));
            }
            catch (Exception ex)
            {
            }

            return null;
        }
    }
}
