﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace Backend.Utilities
{
    public static class Serialization
    {
        public static byte[] ToByteArray(this object obj)
        {
            if (obj == null)
                return null;

            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }

        public static T FromByteArray<T>(this byte[] array) where T : class
        {
            if (array == null)
                return default(T);

            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream(array))
                return formatter.Deserialize(stream) as T;
        }
    }
}
