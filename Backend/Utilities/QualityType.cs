﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Utilities
{
    public enum QualityType
    {
        Unknown = 0,
        Low = 1,
        Medium = 2,
        High = 3
    }
}
