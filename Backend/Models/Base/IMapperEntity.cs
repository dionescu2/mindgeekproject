﻿using AutoMapper;

namespace Backend.Models.Base
{
    public interface IMapperEntity
    {
        void MapperConfiguration(IMapperConfigurationExpression cfg);
    }
}
