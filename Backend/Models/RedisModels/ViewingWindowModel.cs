﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class ViewingWindowModel : IMapperEntity
    {
        public DateTime StartDate { get; set; }
        public string WayToWatch { get; set; }
        public DateTime EndDate { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ViewingWindowResponseModel, ViewingWindowModel>();
        }
    }
}
