﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class VideoModel : IMapperEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public AlternativeModel[] Alternatives { get; set; }
        public string Type { get; set; }
        [IgnoreDataMember]
        public string Url { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<VideoResponseModel, VideoModel>()
                .ForMember(dest => dest.Id, x => x.MapFrom(src => Guid.NewGuid()));
        }
    }
}
