﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;
using Backend.Utilities;


namespace Backend.Models.RedisModels
{
    [Serializable]
    public class AlternativeModel : IMapperEntity
    {
        public Guid Id { get; set; }
        public QualityType Quality { get; set; }
        [IgnoreDataMember]
        public string Url { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<AlternativeResponseModel, AlternativeModel>()
                .ForMember(dest => dest.Id, x => x.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.Quality, x => x.MapFrom(src => src.Quality.ToQualityType()));
        }
    }
}
