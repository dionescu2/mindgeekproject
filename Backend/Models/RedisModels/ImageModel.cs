﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class ImageModel : IMapperEntity
    {
        public Guid Id { get; set; }
        public int H { get; set; }
        public int W { get; set; }
        [IgnoreDataMember]
        public string Url { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ImageResponseModel, ImageModel>()
                .ForMember(dest => dest.Id, x => x.MapFrom(src => Guid.NewGuid()));
        }
    }
}
