﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class ShowCaseModel : IMapperEntity
    {
        public string Body { get; set; }
        public ImageModel[] CardImages { get; set; }
        public CastModel[] Cast { get; set; }
        public string Cert { get; set; }
        public string Class { get; set; }
        public DirectorModel[] Directors { get; set; }
        public int Duration { get; set; }
        public string[] Genres { get; set; }
        public string Headline { get; set; }
        public string Id { get; set; }
        public ImageModel[] KeyArtImages { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Quote { get; set; }
        public int Rating { get; set; }
        public string ReviewAuthor { get; set; }
        public string SkyGoId { get; set; }
        public string SkyGoUrl { get; set; }
        public string Sum { get; set; }
        public string Synopsis { get; set; }
        public string Url { get; set; }
        public VideoModel[] Videos { get; set; }
        public ViewingWindowModel ViewingWindow { get; set; }
        public string Year { get; set; }
        public int OrderNo { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ShowCaseResponseModel, ShowCaseModel>();
        }
    }
}
