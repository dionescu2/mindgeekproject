﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class DirectorModel : IMapperEntity
    {
        public string Name { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<DirectorResponseModel, DirectorModel>();
        }
    }
}
