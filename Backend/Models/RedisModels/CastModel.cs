﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.ResponseModels;

namespace Backend.Models.RedisModels
{
    [Serializable]
    public class CastModel : IMapperEntity
    {
        public string Name { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<CastResponseModel, CastModel>();
        }
    }
}
