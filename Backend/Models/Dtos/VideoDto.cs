﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;
using Backend.Utilities;
using Newtonsoft.Json;

namespace Backend.Models.Dtos
{
    public class VideoDto : IMapperEntity, IBaseMedia
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public IEnumerable<AlternativeDto> Alternatives { get; set; }
        public string Type { get; set; }
        public string File { get; set; }

        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<VideoModel, VideoDto>();
        }
    }
}
