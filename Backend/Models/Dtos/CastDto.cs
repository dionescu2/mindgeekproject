﻿using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;

namespace Backend.Models.Dtos
{
    public class CastDto : IMapperEntity
    {
        public string Name { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<CastModel, CastDto>();
        }
    }
}
