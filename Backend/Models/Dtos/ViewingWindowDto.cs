﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;

namespace Backend.Models.Dtos
{
    public class ViewingWindowDto : IMapperEntity
    {
        public DateTime StartDate { get; set; }
        public string WayToWatch { get; set; }
        public DateTime EndDate { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ViewingWindowModel, ViewingWindowDto>();
        }
    }
}
