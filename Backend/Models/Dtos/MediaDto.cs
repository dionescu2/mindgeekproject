﻿namespace Backend.Models.Dtos
{
    public class MediaDto
    {
        public string File { get; set; }
    }
}
