﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;
using Backend.Utilities;
using Newtonsoft.Json;

namespace Backend.Models.Dtos
{
    public class ImageDto : IMapperEntity, IBaseMedia
    {
        public Guid Id { get; set; }
        public string File { get; set; }
        public int H { get; set; }
        public int W { get; set; }

        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ImageModel, ImageDto>();
        }
    }
}
