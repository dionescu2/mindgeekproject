﻿using System;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;
using Backend.Utilities;
using Newtonsoft.Json;

namespace Backend.Models.Dtos
{
    public class AlternativeDto : IMapperEntity, IBaseMedia
    {
        public Guid Id { get; set; }
        public QualityType Quality { get; set; }
        public string File { get; set; }
        [JsonIgnore]
        public byte[] Raw { get; set; }

        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<AlternativeModel, AlternativeDto>();
        }
    }
}
