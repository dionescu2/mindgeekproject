﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Backend.Models.Base;
using Backend.Models.RedisModels;

namespace Backend.Models.Dtos
{
    public class ShowCaseDto : IMapperEntity
    {
        public string Body { get; set; }
        public IEnumerable<ImageDto> CardImages { get; set; }
        public IEnumerable<CastDto> Cast { get; set; }
        public string Cert { get; set; }
        public string Class { get; set; }
        public IEnumerable<DirectorDto> Directors { get; set; }
        public int Duration { get; set; }
        public string[] Genres { get; set; }
        public string Headline { get; set; }
        public string Id { get; set; }
        public IEnumerable<ImageDto> KeyArtImages { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Quote { get; set; }
        public int Rating { get; set; }
        public string ReviewAuthor { get; set; }
        public string SkyGoId { get; set; }
        public string SkyGoUrl { get; set; }
        public string Sum { get; set; }
        public string Synopsis { get; set; }
        public string Url { get; set; }
        public IEnumerable<VideoDto> Videos { get; set; }
        public ViewingWindowDto ViewingWindow { get; set; }
        public string Year { get; set; }
        public int OrderNo { get; set; }
        public void MapperConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ShowCaseModel, ShowCaseDto>();
        }
    }
}
