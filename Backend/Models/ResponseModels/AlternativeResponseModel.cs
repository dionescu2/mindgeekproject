﻿using System;

namespace Backend.Models.ResponseModels
{
    public class AlternativeResponseModel
    {
        public string Quality { get; set; }
        public string Url { get; set; }
    }
}
