﻿using System;

namespace Backend.Models.ResponseModels
{
    public class DirectorResponseModel
    {
        public string Name { get; set; }
    }
}
