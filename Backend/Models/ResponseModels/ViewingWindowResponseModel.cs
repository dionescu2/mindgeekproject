﻿using System;

namespace Backend.Models.ResponseModels
{
    public class ViewingWindowResponseModel
    {
        public DateTime StartDate { get; set; }
        public string WayToWatch { get; set; }
        public DateTime EndDate { get; set; }
    }
}
