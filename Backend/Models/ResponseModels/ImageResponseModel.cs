﻿using System;

namespace Backend.Models.ResponseModels
{
    public class ImageResponseModel
    {
        public string Url { get; set; }
        public int H { get; set; }
        public int W { get; set; }
    }
}
