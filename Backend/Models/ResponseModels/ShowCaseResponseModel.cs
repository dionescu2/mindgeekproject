﻿using System;
using System.Collections.Generic;

namespace Backend.Models.ResponseModels
{
    public class ShowCaseResponseModel
    {
        public string Body { get; set; }
        public IEnumerable<ImageResponseModel> CardImages { get; set; }
        public IEnumerable<CastResponseModel> Cast { get; set; }
        public string Cert { get; set; }
        public string Class { get; set; }
        public IEnumerable<DirectorResponseModel> Directors { get; set; }
        public int Duration { get; set; }
        public string[] Genres { get; set; }
        public string Headline { get; set; }
        public string Id { get; set; }
        public IEnumerable<ImageResponseModel> KeyArtImages { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Quote { get; set; }
        public int Rating { get; set; }
        public string ReviewAuthor { get; set; }
        public string SkyGoId { get; set; }
        public string SkyGoUrl { get; set; }
        public string Sum { get; set; }
        public string Synopsis { get; set; }
        public string Url { get; set; }
        public IEnumerable<VideoResponseModel> Videos { get; set; }
        public ViewingWindowResponseModel ViewingWindow { get; set; }
        public string Year { get; set; }
    }
}
