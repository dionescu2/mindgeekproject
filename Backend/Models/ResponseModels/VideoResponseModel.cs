﻿using System.Collections.Generic;

namespace Backend.Models.ResponseModels
{
    public class VideoResponseModel
    {
        public string Title { get; set; }
        public IEnumerable<AlternativeResponseModel> Alternatives { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}
