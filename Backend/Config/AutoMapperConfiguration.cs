﻿using System;
using System.Linq;
using AutoMapper;
using Backend.Models.Base;

namespace Backend.Config
{
    public static class AutoMapperConfiguration
    {
        public static void Config(IMapperConfigurationExpression mapperConfigurationExpression)
        {
            var types = AppDomain
                .CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(IMapperEntity).IsAssignableFrom(x) &&
                            x.IsClass && !x.IsAbstract);

            foreach (var type in types)
            {
                var entity = (IMapperEntity)Activator.CreateInstance(type);

                entity.MapperConfiguration(mapperConfigurationExpression);
            }
        }
    }
}
