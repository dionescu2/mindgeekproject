import { ImageModel } from "./image.model";
import { VideoModel } from "./video.model";
import { CastModel } from "./cast.model";
import { ViewWindowModel } from "./view-window.model";

export class ShowCaseModel {
    body: string;
    cardImages: ImageModel[];
    cast: CastModel[];
    cert: string;
    class: string;
    directors: CastModel[];
    duration: number;
    genres: string[];
    headline: string;
    id: string;
    keyArtImages: ImageModel[];
    lastUpdated: Date;
    rating: number;
    url: string;
    videos: VideoModel[];
    viewingWindow: ViewWindowModel; 
    year: string;
    orderNo: number;
}