import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ShowMoreComponent } from './utilities/show-more/show-more.component';
import { MovieInfoComponent } from './movie-info/movie-info.component';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'movie-details/:id', component: MovieInfoComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    MovieInfoComponent,
    ShowMoreComponent,
    HomepageComponent
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
