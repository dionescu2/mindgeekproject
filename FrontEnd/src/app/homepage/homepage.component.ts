import { Component, OnInit } from '@angular/core';
import { ShowCaseModel } from '../models/showcase.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  ShowCases: Array<ShowCaseModel>;
  page: number;
  morePages: boolean;

  constructor(private http: HttpClient, private router: Router) {
    this.page = 0;
    this.ShowCases = new Array<ShowCaseModel>();
    this.morePages = true;
  }

  ngOnInit(){
    this.getElements();
  }

  showMore(id: number) {
    this.router.navigate(['movie-details']);
  }

  getElements() {
    this.http.get<Array<ShowCaseModel>>("http://localhost:51471/api/ShowCase?size=9&page="+this.page).subscribe(x => {
      this.page++;
      if (x == null || x.length != 9) {
        this.morePages = false;
      }
      
      console.log(x);
      x.forEach(sc => {
        sc.cardImages.forEach(ci => {
          if (ci.file) {
            ci.file = 'data:image/jpeg;base64,' + ci.file;
          }
        });
      })
      x.forEach(item => {
        this.ShowCases.push(item)
      })
      this.ShowCases = this.ShowCases.sort(x => x.orderNo);
    },
    error => this.morePages = false);
  }
}
