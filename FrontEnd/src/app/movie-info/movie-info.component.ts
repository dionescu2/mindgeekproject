import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShowCaseModel } from '../models/showcase.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.css']
})
export class MovieInfoComponent implements OnInit {
  private id: string;
  showCase: ShowCaseModel;
  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getItem();
  }

  getItem() {
    this.http.get<ShowCaseModel>("http://localhost:51471/api/ShowCase/GetShowCaseById?id="+this.id).subscribe(x => {
      x.cardImages.forEach(ci => {
        if (ci.file) {
          ci.file = 'data:image/jpeg;base64,' + ci.file;
        }
      });
      x.videos.forEach(v => {
        if (v.file) {
          v.file='data:video/mp4;base64,' + v.file;
        }
      })
      this.showCase = x;
    })
  }

}
