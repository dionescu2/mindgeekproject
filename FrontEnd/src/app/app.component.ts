import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShowCaseModel } from './models/showcase.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FrontEnd';
  constructor() {
    
  }
}
